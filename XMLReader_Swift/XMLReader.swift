import Foundation


extension String {
//   subscript(integerIndex: Int) -> Character {
//      let index = startIndex.advancedBy(integerIndex)
//      return self[index]
//   }


   subscript(index: Int) -> Character {
      get {
         let i = startIndex.advancedBy(index)
         return self[i]
      }
      set {
         self[index] = newValue
      }
   }
   
}



public final class XMLReader: NSObject {

   public static var text = ""

   
   public static var stringsList   = NSMutableArray()
   public static var elementsStack = NSMutableArray()
   


   /*  If tags are not closed:  appends text, returns nil
    else:  returns NSArray of NSDictionaries    or NSDictionary
    */
   public static func addText(text: String) -> NSObject? {
      self.text = text

      
      var element = ""
      var element_text = ""
      
      var element_began = false
      var header_began = false
      
      var element_index = 0
      var element_text_index = 0
      
      var curr_char : Character = "\0"
      var prev_char : Character = "\0"
      var next_char : Character = "\0"
      
      let len = text.characters.count
      
      for offset in 0..<len-1 {
         curr_char = text[offset]
         if offset <= 0 {
            prev_char = "\0"
         } else {
            prev_char = text[offset-1]
         }
         if offset >= len {
            next_char = "\0"
         } else {
            next_char = text[offset+1]
         }
      }

      if (curr_char == "<") {
         if (next_char == "/") {
            element_began = false
            element_text[element_text_index] = "\0"
            element_text_index = 0;
            if (element_text[0] != "\0") {
               element = element + " TEXT_NODE_KEY=" + element_text
               self.stringsList.removeLastObject()
               self.stringsList.addObject("OPEN " + element)
            }
         }
         element_began = true;
         header_began = true;
//         continue;
      }
      
      
      

      return nil
   }
}

/*
 
   for (size_t offset = 0; offset < len; ++offset) {
      
      curr_char = text[offset];
      if (offset <= 0) {
         prev_char = '\0';
      } else prev_char = text[offset-1];
      if (offset >= len) {
         next_char = '\0';
      } else next_char = text[offset+1];
      
      
      if (curr_char == '<') {
         if (next_char == '/') {
            element_began = false;
            element_text[element_text_index] = '\0';
            element_text_index = 0;
            if (element_text[0] != '\0') {
               snprintf(element, 1023, "%s%s%s", element, " TEXT_NODE_KEY=", element_text);
               [self.stringsList removeLastObject];
               [self.stringsList addObject:[NSString stringWithFormat:@"OPEN %s", element]];
            }
         }
         element_began = true;
         header_began = true;
         continue;
      }
      
      if (curr_char == '>') {
         header_began = false;
         element[element_index] = '\0';
         
         if (element[element_index-1] == '/') {
            element[element_index-1] = '\0';
            [self.stringsList addObject:[NSString stringWithFormat:@"OPEN %s", element]];
            [self.stringsList addObject:[NSString stringWithFormat:@"CLOSE %s", element]];
            element_index = 0;
            continue;
         }
         
         if (element[0] == '/') {
            element += 1;
            [self.stringsList addObject:[NSString stringWithFormat:@"CLOSE %s", element]];
            element_index = 0;
            continue;
         }
         
         if (element[0] != '\0') {
            [self.stringsList addObject:[NSString stringWithFormat:@"OPEN %s", element]];
            element_index = 0;
            continue;
         }
         
         
         if (prev_char == '/') {
            element_began = false;
            element_text[element_text_index] = '\0';
            element_text_index = 0;
            if (element_text[0] != '\0') {
               snprintf(element, 1023, "%s%s%s", element, " TEXT_NODE_KEY=", element_text);
               [self.stringsList removeLastObject];
               [self.stringsList addObject:[NSString stringWithFormat:@"OPEN %s", element]];
            }
         }
         continue;
      }
      
      if (element_began) {
         if (header_began) {
            element[element_index] = curr_char;
            ++element_index;
         } else {
            element_text[element_text_index] = curr_char;
            ++element_text_index;
         }
      }
      
   }
   
   
   [self processDicStringList];
   
   return nil;
   }
   
   - (void)processDicStringList {
      NSLog(@"dicStringList::::: \n%@", self.stringsList);
      
      
      
      
      NSMutableDictionary *parentDic = [NSMutableDictionary new];
      
      
      
      for (size_t i = 0; i < self.stringsList.count; ++i) {
         NSArray *parts = [self.stringsList[i] componentsSeparatedByString:@" "];
         
         
         if (parts.count < 2) continue;
         NSString *element = parts[1];
         
         
         if ([parts[0] isEqualToString:@"OPEN"]) {
            [self.elementsStack addObject:element];
            
            NSMutableDictionary *childValueDic = [NSMutableDictionary new];
            if (parts.count == 2) {
               [childValueDic setValue:nil forKey:element];
               continue;
            }
            
            for (size_t i = 2; i < parts.count; ++i) {
               NSArray *keyValue = [self getKeyValuePairFromString:parts[i]];
               if (keyValue.count < 2) continue;
               
               [childValueDic setValue:keyValue[1] forKey:keyValue[0]];
            }
            
            NSMutableDictionary *childDic = [NSMutableDictionary new];
            [childDic setValue:childValueDic forKey:element];
            if (self.elementsStack.count == 1) {
               parentDic = childDic;
            } else {
               NSMutableDictionary *dic;
               
               dic = parentDic;
               for (size_t e = 0; e < self.elementsStack.count; ++e) {
                  if (e == self.elementsStack.count - 1) {
                     [dic addEntriesFromDictionary:childDic];
                  }
                  NSLog(@"elem %@", self.elementsStack[e]);
                  NSLog(@"dic  %@", dic);
                  
                  dic = [dic valueForKey:self.elementsStack[e]];
               }
               
               
               
            }
            
            
            
            NSLog(@"parentDic    %@", parentDic);
            NSLog(@"childDic    %@", childDic);
         }
         
         if ([parts[0] isEqualToString:@"CLOSE"]) {
            if ([self.elementsStack.lastObject isEqualToString:element] ) {
               NSLog(@"elementsStack    %@", self.elementsStack);
               NSLog(@"%@ REMOVED", element);
               [self.elementsStack removeLastObject];
            }
            
            /*
             
             
             */
            
            
         }
      }
      }
      
      
      
      - (NSArray *)getKeyValuePairFromString:(NSString *)string {
         NSMutableArray *out = [string componentsSeparatedByString:@"="].mutableCopy;
         if (out.count < 2) return nil;
         
         for (size_t i = 0; i < 2; ++i) {
            out[i] = [out[i] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            out[i] = [out[i] stringByReplacingOccurrencesOfString:@"'" withString:@""];
         }
         return out;
         
}



*/

